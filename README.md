# Cloudformation

* Author: Alexander Myasnikov
* mailto: myasnikov.alexander.s@gmail.com
* git: https://gitlab.com/amyasnikov/cloudformation_sample



## Create S3 Static website hosting

```
aws cloudformation create-stack \
  --stack-name s3-site-sample \
  --template-body file://s3-site.yaml \
  --timeout-in-minutes 10 \
  --disable-rollback \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameters \
  ParameterKey=S3BucketName,ParameterValue="amyasnikov-s3-hosting-sample"
```

## Create cloudfront

```
aws cloudformation create-stack \
  --stack-name cloudfront-sample \
  --template-body file://cloudfront.yaml \
  --timeout-in-minutes 10 \
  --disable-rollback \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameters \
  ParameterKey=S3DomainName,ParameterValue="amyasnikov-s3-hosting-sample.s3-website.eu-central-1.amazonaws.com"
```



## Update template

```
aws cloudformation update-stack \
  --stack-name cloudfront \
  --template-body file://template.yaml \
  --parameters \
  ParameterKey=S3BucketName,ParameterValue="amyasnikov-cloudfront"
```

## Delete template

```
aws cloudformation delete-stack \
  --stack-name cloudfront 
```

